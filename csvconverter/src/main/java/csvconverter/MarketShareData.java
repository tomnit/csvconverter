package csvconverter;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import csvconverter.domain.MarketShareItem;
import csvconverter.export.IExporter;
import csvconverter.loader.CSVLoader;
import csvconverter.loader.ILoader;

/***
 * Basic implementation of interface for working with market data.
 * 
 * @author tom
 *
 */
public class MarketShareData implements IMarketShareData {
	
	private List<MarketShareItem> data = new ArrayList<MarketShareItem>();	
	private ILoader loader = new CSVLoader();

	public void load(String filePath) {
		try {
			data = loader.load(filePath);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public MarketShareItem getShareData(String vendor, String quarter) {
		// TODO Auto-generated method stub
		return null;
	}

	public void sort(Comparator<MarketShareItem> comparator) {
		data.sort(comparator);
	}

	public int getRow(String vendor) {
		// TODO Auto-generated method stub
		return 0;
	}

	public String export(IExporter exporter) {
		return exporter.export(data);
	}

}
