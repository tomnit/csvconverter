package csvconverter.loader;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import au.com.bytecode.opencsv.CSVReader;
import csvconverter.domain.MarketShareItem;

/***
 * ILoader implementation for loading CSV data.
 * 
 * @author tom
 *
 */
public class CSVLoader implements ILoader {
	
	private CSVReader reader;

	public List<MarketShareItem> load(String filePath) throws FileNotFoundException, IOException {
		reader = new CSVReader(new FileReader(filePath));		
		List<MarketShareItem> items = new ArrayList<MarketShareItem>();
		MarketShareItem item;		
		int lineIndex = 0;
		String[] nextLine;
		while((nextLine = reader.readNext()) != null) {
			lineIndex++;
			item = new MarketShareItem();
			item.setId(lineIndex);
			item.setVendor(nextLine[0]);
			item.setUnits(Integer.parseInt(nextLine[1]));
			item.setShare(Double.parseDouble(nextLine[2]));
			items.add(item);
		}
		return items;
	}

}
