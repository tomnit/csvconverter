package csvconverter.loader;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import csvconverter.domain.MarketShareItem;

/***
 * Class for load data from file into memory.
 * 
 * Mapping loaded data to objects.
 * 
 * @author tom
 *
 */
public interface ILoader {
	
	/***
	 * Load data from file.
	 * 
	 * @param filePath Path of file with data
	 * @return Market share items
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	List<MarketShareItem> load(String filePath) throws FileNotFoundException, IOException;

}
