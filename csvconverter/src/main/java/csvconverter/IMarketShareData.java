package csvconverter;

import java.util.Comparator;

import csvconverter.domain.MarketShareItem;
import csvconverter.export.IExporter;

/***
 * Market data interface.
 * 
 * Start point for working with market data.
 * 
 * @author tom
 *
 */
public interface IMarketShareData {
	
	/***
	 * Loads data from file to memory.
	 * 
	 * @param filePath Absolute path to data source file. File must be in valid CSV format.
	 */
	void load(String filePath);
	
	
	/***
	 * Searching in market data with defined attributes 
	 * 
	 * @param vendor Define vendor title of data item
	 * @param quarter Define quarter of market data item
	 * @return Data item or null if nothing is found
	 */
	MarketShareItem getShareData(String vendor, String quarter);
		
	
	/***
	 * Sort loaded data.
	 * 
	 * @param comparator Comparator for sorting defined data
	 */
	void sort(Comparator<MarketShareItem> comparator);
	
	
	/***
	 * Return index of row with defined vendor
	 * 
	 * @param vendor
	 * @return
	 */
	int getRow(String vendor);
	
	
	/***
	 * Export data to defined format
	 * 
	 * @param exporter Define data output format
	 * @return Data in defined format
	 */
	String export(IExporter exporter);

}
