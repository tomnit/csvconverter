package csvconverter.domain;


/***
 * Basic class representation of market data unit.
 * 
 * @author tom
 *
 */
public class MarketShareItem {
	
	private int id;
	private String vendor;
	private int units;
	private double share;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getVendor() {
		return vendor;
	}
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}
	public int getUnits() {
		return units;
	}
	public void setUnits(int units) {
		this.units = units;
	}
	public double getShare() {
		return share;
	}
	public void setShare(double share) {
		this.share = share;
	}	

}
