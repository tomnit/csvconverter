package csvconverter.utils;

import java.util.Comparator;

import csvconverter.comparator.UnitsComparator;
import csvconverter.comparator.VendorComparator;
import csvconverter.domain.MarketShareItem;

public enum SortType {
	
	VENDOR(new VendorComparator()),
	UNITS(new UnitsComparator());
	
	private Comparator<MarketShareItem> comparator;
	
	private SortType(Comparator<MarketShareItem> comparator) {
		this.comparator = comparator;
	}
	
	public Comparator<MarketShareItem> getComparator() {
		return comparator;
	}

}
