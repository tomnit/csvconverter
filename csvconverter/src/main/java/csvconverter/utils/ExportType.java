package csvconverter.utils;

import csvconverter.export.CSVExporter;
import csvconverter.export.ExcelExporter;
import csvconverter.export.HTMLExporter;
import csvconverter.export.IExporter;

public enum ExportType {
	
	HTML(new HTMLExporter()),
	EXCEL(new ExcelExporter()),
	CSV(new CSVExporter());
	
	private IExporter exporter;	
	
	private ExportType(IExporter exporter) {
		this.exporter = exporter;
	}
	
	public IExporter getExporter() {
		return exporter;
	}

}
