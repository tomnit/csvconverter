package csvconverter.export;

import java.util.List;

import csvconverter.domain.MarketShareItem;

/***
 * Export data to defined format
 * 
 * @author tom
 *
 */
public interface IExporter {
	
	/***
	 * Export data to defined format
	 * 
	 * @param data Market data to export
	 * @return Data representation in string with defined format
	 */
	String export(List<MarketShareItem> data);

}
