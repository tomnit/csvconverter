package csvconverter.export;

import java.util.List;

import csvconverter.domain.MarketShareItem;
import csvconverter.htmlcreator.Document;
import csvconverter.htmlcreator.Tag;

public class HTMLExporter implements IExporter {
	
	public static final String TITLE = "Market Share Data";

	public String export(List<MarketShareItem> data) {
		Document document = new Document();
		
		// define header		
		Tag header = new Tag("header");
		document.setHeader(header);
		
		doAddTag("title", TITLE, header);
		
		
		// define body
		Tag body = new Tag("body");
		document.setBody(body);		
		
		Tag table = new Tag("table");
		body.addElement(table);
		
		// add headings
		Tag tableRow = new Tag("tr");
		doAddHeading("Vendor", tableRow);
		doAddHeading("Units", tableRow);
		doAddHeading("Share", tableRow);
		table.addElement(tableRow);
		
		// add market data
		for(MarketShareItem item : data) {
			tableRow = new Tag("tr");			
			doAddTableCell(item.getVendor(), tableRow);
			doAddTableCell(Integer.toString(item.getUnits()), tableRow);
			doAddTableCell(Double.toString(item.getShare()), tableRow);			
			table.addElement(tableRow);
		}
		
		// add summary
		tableRow = new Tag("tr");
		doAddTableCell("Total", tableRow);
		// doAddTableCell("Total", tableRow);
		// doAddTableCell("Total", tableRow);
		
		return document.toString();
	}
	
	private void doAddHeading(String heading, Tag tableRow) {
		doAddTag("th", heading, tableRow);
	}
	
	private void doAddTableCell(String data, Tag tableRow) {
		doAddTag("td", data, tableRow);
	}
	
	private void doAddTag(String tagTitle, String data, Tag parentTag) {
		Tag tag = new Tag(tagTitle);
		tag.addElement(data);
		parentTag.addElement(tag);
	}


}
