package csvconverter.htmlcreator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/***
 * Class representation for HTML tag. 
 * 
 * @author tom
 *
 */
public class Tag {
	
	private String title;
	private Map<String, String> attributes = new HashMap<String, String>();
	private List<Object> internalElements = new ArrayList<Object>();
	
	public Tag(String title) {
		this.title = title;
	}
	
	
	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}
	
	
	public void addAttribute(String key, String value) {
		attributes.put(key, value);
	}
	
	
	public void removeAttribute(String key) {
		attributes.remove(key);
	}
	
	/***
	 * Add elements into tag
	 * 
	 * These elements can be other tags or strings.
	 * 
	 * @param element 
	 */
	public void addElement(Object element) {
		internalElements.add(element);
	}
	
	
	public void removeElement(Object element) {
		internalElements.remove(element);
	}
	
	
	public void removeElement(int index) {
		internalElements.remove(index);
	}	


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		
		// create open tag
		builder.append("<" + title);
		if(attributes.size() > 0) {
			for(String attributeKey : attributes.keySet()) {
				builder.append(" ");
				builder.append(attributeKey + "=\"" + attributes.get(attributeKey) + "\"");
			}
		}
		
		if(internalElements.size() == 0) {
			builder.append("/>");
			return builder.toString();
		}
		
		// write 
		for(Object tag : internalElements) {
			builder.append(tag.toString());
		}
		
		builder.append("</" + title + ">");
		return builder.toString();
	}	

}
