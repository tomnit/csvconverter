package csvconverter.htmlcreator;


/***
 * Class representation of HTML document.
 * 
 * @author tom
 *
 */
public class Document {
	
	private Tag header;
	private Tag body;	
	
	
	public Tag getHeader() {
		return header;
	}


	public void setHeader(Tag header) {
		this.header = header;
	}


	public Tag getBody() {
		return body;
	}


	public void setBody(Tag body) {
		this.body = body;
	}


	@Override
	public String toString() {
		Tag htmlTag = new Tag("html");
		htmlTag.addElement(header);
		htmlTag.addElement(body);
		return htmlTag.toString();
	}

}
