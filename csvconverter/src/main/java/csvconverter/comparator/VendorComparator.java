package csvconverter.comparator;

import java.util.Comparator;

import csvconverter.domain.MarketShareItem;

public class VendorComparator implements Comparator<MarketShareItem> {

	public int compare(MarketShareItem o1, MarketShareItem o2) {
		return o1.getVendor().compareToIgnoreCase(o2.getVendor());
	}

}
