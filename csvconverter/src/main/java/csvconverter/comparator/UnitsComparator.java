package csvconverter.comparator;

import java.util.Comparator;

import csvconverter.domain.MarketShareItem;

public class UnitsComparator implements Comparator<MarketShareItem> {

	public int compare(MarketShareItem o1, MarketShareItem o2) {
		return o1.getUnits() - o2.getUnits(); 
	}

}
